#include "stepper/stepper_dummy.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv) {
  printf("creating stepper\n");
  stepper_dummy_t *stepper = stepper_dummy_create(.03, true);
  if (stepper == NULL) {
    exit(100);
  }

  printf("move deg\n");
  stepper_dummy_move_deg(stepper, 10);
  long steps;
  stepper_dummy_get_step(stepper, &steps);
  printf("steps: %li\n", steps);

  stepper_dummy_move_rad(stepper, -3.2);
  stepper_dummy_get_step(stepper, &steps);
  printf("steps: %li\n", steps);
  stepper_dummy_move_steps(stepper, 150);
  stepper_dummy_move_steps(stepper, -150);
  stepper_dummy_move_steps(stepper, 150);
  stepper_dummy_wait_for_done(stepper);

  printf("destroying stepper\n");
  stepper_dummy_destroy(&stepper);
  return 0;
}
