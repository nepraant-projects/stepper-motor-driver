#include "stepper_queue.h"
#include <stdio.h>
#include <stdlib.h>

stepper_queue_t *stepper_queue_create() {
  stepper_queue_t *queue = (stepper_queue_t *)malloc(sizeof(stepper_queue_t));
  if (queue == NULL) {
    return NULL;
  }

  if (mtx_init(&queue->mutex, mtx_plain) != thrd_success) {
    free(queue);
    return NULL;
  }

  queue->length = 0;
  queue->begin = NULL;
  queue->end = NULL;

  return queue;
}

bool stepper_queue_delete(stepper_queue_t **queue) {
  if (queue == NULL || *queue == NULL) {
    return false;
  }

  // pop all stored data
  while ((*queue)->length > 0) {
    stepper_queue_pop(*queue, NULL);
  }

  mtx_destroy(&((*queue)->mutex));
  free(*queue);
  *queue = NULL;

  return true;
}

bool stepper_queue_push(stepper_queue_t *queue, stepper_queue_data_t data,
                        int priority) {
  if (queue == NULL) {
    return false;
  }

  mtx_lock(&queue->mutex);

  stepper_queue_node_t *node =
      (stepper_queue_node_t *)malloc(sizeof(stepper_queue_node_t));
  if (node == NULL) {
    return false;
  }

  node->data = data;
  node->priority = priority;

  if (queue->length == 0) {
    node->prev = NULL;
    node->next = NULL;
    queue->begin = node;
    queue->end = node;
  } else if (priority <= queue->end->priority) {
    node->next = NULL;
    node->prev = queue->end;
    queue->end->next = node;
    queue->end = node;
  } else if (priority > queue->begin->priority) {
    node->prev = NULL;
    node->next = queue->begin;
    queue->begin->prev = node;
    queue->begin = node;
  } else {
    // get node before
    stepper_queue_node_t *prev_node = queue->end;
    while (prev_node->priority > priority) {
      prev_node = prev_node->prev;
    }

    node->prev = prev_node;
    node->next = prev_node->next;
    prev_node->next = node;
    ((stepper_queue_node_t *)node->next)->prev = node;
  }

  queue->length++;

  mtx_unlock(&queue->mutex);

  return true;
}

bool stepper_queue_pop(stepper_queue_t *queue, stepper_queue_data_t *data_ptr) {
  if (queue == NULL || queue->length < 1) {
    return false;
  }

  mtx_lock(&queue->mutex);

  stepper_queue_node_t *node = queue->begin;

  if (queue->length == 1) {
    queue->begin = NULL;
    queue->end = NULL;
  } else {
    stepper_queue_node_t *next_node = queue->begin->next;
    next_node->prev = NULL;
    queue->begin = next_node;
  }

  if (data_ptr != NULL) {
    *data_ptr = node->data;
  }

  free(node);
  queue->length--;

  mtx_unlock(&queue->mutex);

  return true;
}

bool stepper_queue_is_empty(stepper_queue_t *queue) {
  if (queue == NULL) {
    return false;
  }

  mtx_lock(&queue->mutex);
  bool is_empty = queue->length < 1;
  mtx_unlock(&queue->mutex);

  return is_empty;
}