#ifndef __STEPPER_QUEUE__
#define __STEPPER_QUEUE__

#include <stdbool.h>
#include <threads.h>

/**
 * @brief Types of stepper queue entry data
 *
 */
enum {
  STEPPER_QUEUE_ENTRY_MOVE_RAD,
  STEPPER_QUEUE_ENTRY_MOVE_DEG,
  STEPPER_QUEUE_ENTRY_MOVE_STEPS,
  STEPPER_QUEUE_ENTRY_STOP
};

/**
 * @brief Stepper queue entry data values
 *
 */
typedef struct {
  int type;
  union {
    double angle;
    int steps;
  } value;
} stepper_queue_data_t;

/**
 * @brief Stepper queue node - for linked list
 *
 */
typedef struct {
  int priority;
  stepper_queue_data_t data;
  void *prev;
  void *next;
} stepper_queue_node_t;

/**
 * @brief Stepper queue - used for commands for the stepper
 *
 */
typedef struct {
  mtx_t mutex;

  int length;
  stepper_queue_node_t *begin;
  stepper_queue_node_t *end;
} stepper_queue_t;

/**
 * @brief Allocate and init new stepper queue structure
 *
 * @return stepper_queue_t*
 */
stepper_queue_t *stepper_queue_create();

/**
 * @brief Delete stepper queue
 *
 * @param queue
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_queue_delete(stepper_queue_t **queue);

/**
 * @brief Push data to the queue
 *
 * @param queue
 * @param data
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_queue_push(stepper_queue_t *queue, stepper_queue_data_t data,
                        int priority);

/**
 * @brief Pop data from the queue
 *
 * @param queue
 * @param data_ptr data will be written to this pointer - if NULL data is
 *                 destroyed
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_queue_pop(stepper_queue_t *queue, stepper_queue_data_t *data_ptr);

/**
 * @brief Check if queue is empty
 *
 * @param queue
 * @return true queue is empty
 * @return false queue is not empry or ERROR
 */
bool stepper_queue_is_empty(stepper_queue_t *queue);

#endif
