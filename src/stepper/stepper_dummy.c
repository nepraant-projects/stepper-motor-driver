#include "stepper_dummy.h"
#include "stepper_queue.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <threads.h>
#include <unistd.h>

int stepper_func(void *data_ptr);

double rad_to_deg(double rad) { return 180.0 * rad / M_PI; }

double deg_to_rad(double deg) { return M_PI * deg / 180.0; }

stepper_dummy_t *stepper_dummy_create(double angle_per_step_rad,
                                      bool halfstepping) {
  stepper_dummy_t *stepper = (stepper_dummy_t *)malloc(sizeof(stepper_dummy_t));
  if (stepper == NULL) {
    return NULL;
  }

  // init data
  stepper->data.alive = true;
  stepper->data.queue = stepper_queue_create();
  if (stepper->data.queue == NULL) {
    free(stepper);
    return NULL;
  }
  stepper->data.halfstepping = halfstepping;
  stepper->data.step = 0;
  stepper->data.angle_per_step_rad =
      halfstepping ? angle_per_step_rad / 2.0 : angle_per_step_rad;
  stepper->data.angle_target_rad = 0;
  for (int i = 0; i < 4; ++i) {
    stepper->data.states[i] =
        halfstepping ? stepper_halfsteps[0][i] : stepper_steps[0][i];
  }

  // init mutex
  if (mtx_init(&stepper->data.mutex, mtx_plain) != thrd_success) {
    stepper_queue_delete(&stepper->data.queue);
    free(stepper);
  }

  // init conds
  if (cnd_init(&stepper->data.cond) != thrd_success) {
    stepper_queue_delete(&stepper->data.queue);
    mtx_destroy(&stepper->data.mutex);
    free(stepper);
  }
  if (cnd_init(&stepper->data.cond_done) != thrd_success) {
    stepper_queue_delete(&stepper->data.queue);
    cnd_destroy(&stepper->data.cond);
    mtx_destroy(&stepper->data.mutex);
    free(stepper);
  }

  // create stepper thread
  if (thrd_create(&stepper->thread, stepper_func, &stepper->data) !=
      thrd_success) {
    mtx_destroy(&stepper->data.mutex);
    stepper_queue_delete(&stepper->data.queue);
    free(stepper);
  }

  return stepper;
}

bool stepper_dummy_destroy(stepper_dummy_t **stepper) {
  if (stepper == NULL || *stepper == NULL) {
    return false;
  }

  // unalive stepper
  mtx_lock(&(*stepper)->data.mutex);
  (*stepper)->data.alive = false;
  mtx_unlock(&(*stepper)->data.mutex);
  cnd_signal(&(*stepper)->data.cond);

  // join thread
  thrd_join((*stepper)->thread, NULL);

  // delete data
  stepper_queue_delete(&(*stepper)->data.queue);
  mtx_destroy(&(*stepper)->data.mutex);
  cnd_destroy(&(*stepper)->data.cond);
  cnd_destroy(&(*stepper)->data.cond_done);
  free(*stepper);
  *stepper = NULL;

  return true;
}

bool stepper_dummy_move_rad(stepper_dummy_t *stepper, double angle_rad) {
  if (stepper == NULL) {
    return false;
  }

  stepper_queue_data_t data;
  data.type = STEPPER_QUEUE_ENTRY_MOVE_RAD;
  data.value.angle = angle_rad;
  stepper_queue_push(stepper->data.queue, data, 10);
  cnd_signal(&stepper->data.cond);

  return true;
}

bool stepper_dummy_move_deg(stepper_dummy_t *stepper, double angle_deg) {
  if (stepper == NULL) {
    return false;
  }

  stepper_queue_data_t data;
  data.type = STEPPER_QUEUE_ENTRY_MOVE_DEG;
  data.value.angle = angle_deg;
  stepper_queue_push(stepper->data.queue, data, 10);
  cnd_signal(&stepper->data.cond);

  return true;
}

bool stepper_dummy_move_steps(stepper_dummy_t *stepper, int steps) {
  if (stepper == NULL) {
    return false;
  }

  stepper_queue_data_t data;
  data.type = STEPPER_QUEUE_ENTRY_MOVE_STEPS;
  data.value.steps = steps;
  stepper_queue_push(stepper->data.queue, data, 10);
  cnd_signal(&stepper->data.cond);

  return true;
}

bool stepper_dummy_stop(stepper_dummy_t *stepper) {
  if (stepper == NULL) {
    return false;
  }

  stepper_queue_data_t data;
  data.type = STEPPER_QUEUE_ENTRY_STOP;

  stepper_queue_push(stepper->data.queue, data, 20);
  cnd_signal(&stepper->data.cond);
  return true;
}

void stepper_print_info(stepper_dummy_data_t *data) {
  printf("%6li %6.2lf ", data->step, data->step * data->angle_per_step_rad);
  for (int i = 0; i < STEPPER_STEPS_CNT; ++i) {
    printf("%c", data->states[i] ? 'X' : '-');
  }
  printf("\n");
}

void stepper_set_states(stepper_dummy_data_t *data) {
  int step = data->step %
             (data->halfstepping ? STEPPER_HALFSTEPS_CNT : STEPPER_STEPS_CNT);
  if (step < 0) {
    step += data->halfstepping ? STEPPER_HALFSTEPS_CNT : STEPPER_STEPS_CNT;
  }
  for (int i = 0; i < STEPPER_STEPS_CNT; ++i) {
    data->states[i] = data->halfstepping ? stepper_halfsteps[step][i]
                                         : stepper_steps[step][i];
  }

  stepper_print_info(data);
  usleep(1000);
}

void stepper_step_up(stepper_dummy_data_t *data) {
  data->step++;
  stepper_set_states(data);
}

void stepper_step_down(stepper_dummy_data_t *data) {
  data->step--;
  stepper_set_states(data);
}

void stepper_move(stepper_dummy_data_t *data) {
  double move_angle =
      data->angle_target_rad - data->step * data->angle_per_step_rad;

  int move_steps = move_angle / data->angle_per_step_rad;

  if (move_steps > 0) {
    for (int i = 0; i < move_steps; ++i) {
      stepper_step_up(data);
    }
  }

  if (move_steps < 0) {
    for (int i = 0; i > move_steps; --i) {
      stepper_step_down(data);
    }
  }
}

int stepper_func(void *data_ptr) {
  stepper_dummy_data_t *data = (stepper_dummy_data_t *)data_ptr;

  mtx_lock(&data->mutex);
  while (true) {
    // quit if not alive
    cnd_wait(&data->cond, &data->mutex);
    if (!data->alive) {
      break;
    }

    while (data->queue->length > 0) {
      stepper_queue_data_t q_data;
      stepper_queue_pop(data->queue, &q_data);

      switch (q_data.type) {
      case STEPPER_QUEUE_ENTRY_MOVE_RAD:
        data->angle_target_rad += q_data.value.angle;
        break;
      case STEPPER_QUEUE_ENTRY_MOVE_DEG:
        data->angle_target_rad += deg_to_rad(q_data.value.angle);
        break;
      case STEPPER_QUEUE_ENTRY_MOVE_STEPS:
        data->angle_target_rad +=
            data->halfstepping
                ? q_data.value.steps * data->angle_per_step_rad * 2.0
                : q_data.value.steps * data->angle_per_step_rad;
        break;
      case STEPPER_QUEUE_ENTRY_STOP:
        // empty queue
        while (stepper_queue_pop(data->queue, NULL))
          ;
        break;
      default:
        break;
      }

      stepper_move(data);
    }
    cnd_broadcast(&data->cond_done);
  }
  mtx_unlock(&data->mutex);

  return 0;
}

bool stepper_dummy_get_angle_rad(stepper_dummy_t *stepper, double *angle_ptr) {
  if (stepper == NULL) {
    return false;
  }

  mtx_lock(&stepper->data.mutex);
  double angle = stepper->data.step * stepper->data.angle_per_step_rad;
  mtx_unlock(&stepper->data.mutex);

  if (angle_ptr != NULL) {
    *angle_ptr = angle;
  }

  return true;
}

bool stepper_dummy_get_angle_deg(stepper_dummy_t *stepper, double *angle_ptr) {
  if (stepper == NULL) {
    return false;
  }

  mtx_lock(&stepper->data.mutex);
  double angle =
      rad_to_deg(stepper->data.step * stepper->data.angle_per_step_rad);
  mtx_unlock(&stepper->data.mutex);

  if (angle_ptr != NULL) {
    *angle_ptr = angle;
  }

  return true;
}

bool stepper_dummy_get_step(stepper_dummy_t *stepper, long *step_ptr) {
  if (stepper == NULL) {
    return false;
  }

  mtx_lock(&stepper->data.mutex);
  long step = stepper->data.step;
  mtx_unlock(&stepper->data.mutex);

  if (step_ptr != NULL) {
    *step_ptr = step;
  }

  return true;
}

bool stepper_dummy_wait_for_done(stepper_dummy_t *stepper) {
  if (stepper == NULL) {
    return false;
  }

  if (stepper_queue_is_empty(stepper->data.queue)) {
    return true;
  }

  mtx_t mutex;
  if (mtx_init(&mutex, mtx_plain) != thrd_success) {
    return false;
  }

  mtx_lock(&mutex);
  cnd_wait(&stepper->data.cond_done, &mutex);
  mtx_unlock(&mutex);
  mtx_destroy(&mutex);

  return true;
}
