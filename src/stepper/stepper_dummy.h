#ifndef __STEPPER_DUMMY__
#define __STEPPER_DUMMY__

#include "stepper_queue.h"
#include <stdbool.h>
#include <threads.h>

// stepper motor steps
#define STEPPER_STEPS_CNT 4
static const bool stepper_steps[STEPPER_STEPS_CNT][4] = {
    {true, false, false, false},
    {false, true, false, false},
    {false, false, true, false},
    {false, false, false, true}};

// steper motor halfsteps sequence
#define STEPPER_HALFSTEPS_CNT 8
static const bool stepper_halfsteps[STEPPER_HALFSTEPS_CNT][4] = {
    {true, false, false, false}, {true, true, false, false},
    {false, true, false, false}, {false, true, true, false},
    {false, false, true, false}, {false, false, true, true},
    {false, false, false, true}, {true, false, false, true}};

/**
 * @brief stepper motor data structure
 *
 */
typedef struct {
  mtx_t mutex;
  cnd_t cond;
  cnd_t cond_done;
  bool alive;

  stepper_queue_t *queue;

  bool halfstepping;
  long step;
  double angle_per_step_rad;
  double angle_target_rad;

  bool states[4];
} stepper_dummy_data_t;

/**
 * @brief stepper motor structure
 *
 */
typedef struct {
  stepper_dummy_data_t data;
  thrd_t thread;
} stepper_dummy_t;

/**
 * @brief create new stepper motor structure
 *
 * @param angle_per_step_rad angle per one step in radians
 * @param halfstepping enable halfstepping
 * @return stepper_dummy_t* NULL on error
 */
stepper_dummy_t *stepper_dummy_create(double angle_per_step_rad,
                                      bool halfstepping);

/**
 * @brief destroy stepper motor structure
 *
 * @param stepper
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_dummy_destroy(stepper_dummy_t **stepper);

/**
 * @brief send move to stepper motor in radians
 *
 * @param stepper
 * @param angle_rad angle in radians
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_dummy_move_rad(stepper_dummy_t *stepper, double angle_rad);

/**
 * @brief send move to stepper motor in degrees
 *
 * @param stepper
 * @param angle_deg angle in degrees
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_dummy_move_deg(stepper_dummy_t *stepper, double angle_deg);

/**
 * @brief send move to stepper motor in steps
 *
 * @param stepper
 * @param steps number of steps to perform
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_dummy_move_steps(stepper_dummy_t *stepper, int steps);

/**
 * @brief send stop now to stepper motor
 *
 * @param stepper
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_dummy_stop(stepper_dummy_t *stepper);

/**
 * @brief get stepper angle in radians
 *
 * @param stepper
 * @param angle_ptr
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_dummy_get_angle_rad(stepper_dummy_t *stepper, double *angle_ptr);

/**
 * @brief get stepper angle in radians
 *
 * @param stepper
 * @param angle_ptr
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_dummy_get_angle_deg(stepper_dummy_t *stepper, double *angle_ptr);

/**
 * @brief get stepper step
 *
 * @param stepper
 * @param step_ptr
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_dummy_get_step(stepper_dummy_t *stepper, long *step_ptr);

/**
 * @brief wait until stepper finishes jobs
 *
 * @param stepper
 * @return true SUCCESS
 * @return false ERROR
 */
bool stepper_dummy_wait_for_done(stepper_dummy_t *stepper);

#endif
